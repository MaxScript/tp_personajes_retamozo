﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TP_Personajes
{
    class ACCESO
    {
        SqlConnection conexion = new SqlConnection();

        public ACCESO()
        {

        }
        public void Abrir()
        {
            conexion.ConnectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=ACT_2;Data Source=.\SQLEXPRESS";
            conexion.Open();
        }
        
        public void Cerrar()
        {
            conexion.Close();
            GC.Collect();
        }
        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();
            comando.CommandType = CommandType.Text;
            comando.CommandText = sql;
            comando.Connection = conexion;

            return comando;
        }

        
        public int Escribir(string sql)
        {
            int filasAfectadas = 0;
            Abrir();

            SqlCommand comando = CrearComando(sql);

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                filasAfectadas = -1;
            }

            Cerrar();

            return filasAfectadas;
        }
        public SqlDataReader Leer(string sql)
        {
            SqlDataReader lector = null;

            SqlCommand comando = CrearComando(sql);

            lector = comando.ExecuteReader();


            return lector;
        }

        public int DevolverEscalar(string sql)
        {
            Abrir();

            int id;

            SqlCommand comando = CrearComando(sql);

            id = int.Parse(comando.ExecuteScalar().ToString());


            Cerrar();
            return id;
        }
    }
}
