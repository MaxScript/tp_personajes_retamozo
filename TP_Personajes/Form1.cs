﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Personajes
{
    public partial class Form1 : Form
    {
        ACCESO acceso = new ACCESO();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarPersonajes();
        }

        void EnlazarPersonajes()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = PERSONAJE.ListarPersonajes();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = RAZA.ListarRaza();
            comboBox2.DataSource = null;
            comboBox2.DataSource = HABILIDAD.ListarHabilidad();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Frm_Raza frm_raza = new Frm_Raza();

            frm_raza.ShowDialog();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PERSONAJE personaje = new PERSONAJE();

            RAZA raza = (RAZA)comboBox1.SelectedItem;

            personaje.Nombre_p = textBox1.Text;
            personaje.Fuerza = int.Parse(textBox2.Text);
            personaje.Puntos = int.Parse(textBox3.Text);
            personaje.Nivel = int.Parse(textBox4.Text);
            personaje.Experiencia = int.Parse(textBox5.Text);
            personaje.Raza = raza;

            personaje.Insertar();
            
            EnlazarPersonajes();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RAZA raza = (RAZA)comboBox1.SelectedItem;

            string sql = "Update personaje set Nombre_p = '" + textBox1.Text;
            sql += "', Fuerza = " + textBox2.Text + ", Puntos = " + textBox3.Text + ", Nivel = " + textBox4.Text 
            + ", Experiencia =" + textBox5.Text + ", ID_RAZA = " + raza.ID.ToString() + " where ID_PERSONAJE = " + lblID_Personaje.Text;

            ACCESO acceso = new ACCESO();

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Ok");
            }
            

            EnlazarPersonajes();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                string sql = "Delete from personaje_habilidad where ID_PERSONAJE =" + personaje.ID_Personaje.ToString() + "";

                if (acceso.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Ok");
                }
            }
            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                string sql = "Delete from personaje where ID_PERSONAJE =" + personaje.ID_Personaje.ToString() + "";

                if (acceso.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Ok");
                }
            }
            EnlazarPersonajes();
            acceso = null;
            GC.Collect();
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            PERSONAJE personajeElegido = (PERSONAJE)dataGridView1.Rows[e.RowIndex].DataBoundItem;

            lblID_Personaje.Text = personajeElegido.ID_Personaje.ToString();
            textBox1.Text = personajeElegido.Nombre_p;
            textBox2.Text = personajeElegido.Fuerza.ToString();
            textBox3.Text = personajeElegido.Puntos.ToString();
            textBox4.Text = personajeElegido.Nivel.ToString();
            textBox5.Text = personajeElegido.Experiencia.ToString();
            RAZA r = (from RAZA rr in comboBox1.Items
                      where rr.ID == personajeElegido.Raza.ID
                      select rr).First();
            comboBox1.SelectedItem = null;
            comboBox1.SelectedItem = r;

            personajeElegido.ObtenerHabilidades();

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = personajeElegido.Habilidades;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();
            HABILIDAD habilidad = (HABILIDAD)comboBox2.SelectedItem;
            

            string sql = "INSERT INTO PERSONAJE_HABILIDAD (ID_PERSONAJE, ID_HABILIDAD) VALUES ";

            sql += "( "+ lblID_Personaje.Text +", " + habilidad.ID.ToString() + ")";
            
            acceso.Escribir(sql);
            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                personaje.ObtenerHabilidades();

                dataGridView2.DataSource = null;
                dataGridView2.DataSource = personaje.Habilidades;
            }
            acceso = null;
            GC.Collect();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                string sql = "Delete from PERSONAJE_HABILIDAD where ID_PERSONAJE = " + personaje.ID_Personaje.ToString();
                foreach(DataGridViewRow registro2 in dataGridView2.SelectedRows)
                {
                    HABILIDAD habilidad = (HABILIDAD)registro2.DataBoundItem;
                    sql += "and ID_HABILIDAD =" + habilidad.ID.ToString();
                    acceso.Escribir(sql);
                    personaje.ObtenerHabilidades();

                    dataGridView2.DataSource = null;
                    dataGridView2.DataSource = personaje.Habilidades;
                }
                
            }

            
            acceso = null;
            GC.Collect();
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Frm_Habilidad frm_habilidad = new Frm_Habilidad();
            frm_habilidad.ShowDialog();
        }
    }
}
