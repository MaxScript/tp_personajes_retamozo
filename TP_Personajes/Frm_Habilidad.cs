﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Personajes
{
    public partial class Frm_Habilidad : Form
    {
        public Frm_Habilidad()
        {
            InitializeComponent();
        }

        private void Frm_Habilidad_Load(object sender, EventArgs e)
        {
            EnlazarHabilidad();
        }
        void EnlazarHabilidad()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = HABILIDAD.ListarHabilidad();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HABILIDAD habilidad = new HABILIDAD();

            habilidad.Nombre = textBox1.Text;
            habilidad.Mana = int.Parse(textBox2.Text);
            habilidad.Daño = int.Parse(textBox3.Text);

            habilidad.Insertar();

            EnlazarHabilidad();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = "Update HABILIDAD set Nombre = '" + textBox1.Text;
            sql += "', Mana = " + textBox2.Text + ", Daño = " + textBox3.Text + " where ID_Habilidad = " + lblHab.Text;

            ACCESO acceso = new ACCESO();

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Ok");
            }
            EnlazarHabilidad();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            HABILIDAD habilidad = (HABILIDAD)dataGridView1.Rows[e.RowIndex].DataBoundItem;

            lblHab.Text = habilidad.ID.ToString();
            textBox1.Text = habilidad.Nombre;
            textBox2.Text = habilidad.Mana.ToString();
            textBox3.Text = habilidad.Daño.ToString();


        }

        private void button3_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                HABILIDAD habilidad = (HABILIDAD)registro.DataBoundItem;

                string sql = "Delete from HABILIDAD where ID_HABILIDAD =" + habilidad.ID.ToString();

                if (acceso.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error. Verificar que la habilidad a borrar no esta siendo ocupada por un personaje.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Ok");
                }

            }

            EnlazarHabilidad();
            acceso = null;
            GC.Collect();
        }

        
    }
}
