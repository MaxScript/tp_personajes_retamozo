﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Personajes
{
    public partial class Frm_Raza : Form
    {
        public Frm_Raza()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RAZA raza = new RAZA();

            raza.Nombre_Raza = textBox1.Text;
            raza.Insertar();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sql = "Update RAZA set Nombre_Raza = '" + textBox1.Text + "' where id_raza = " + lblRaza.Text;
            ACCESO acceso = new ACCESO();

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Ok");
            }
            EnlazarRaza();
        }

        private void Frm_Raza_Load(object sender, EventArgs e)
        {
            EnlazarRaza();
        }
        void EnlazarRaza()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = RAZA.ListarRaza();
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            RAZA raza = (RAZA)dataGridView1.Rows[e.RowIndex].DataBoundItem;

            lblRaza.Text = raza.ID.ToString();
            textBox1.Text = raza.Nombre_Raza;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            foreach (DataGridViewRow registro in dataGridView1.SelectedRows)
            {
                RAZA raza = (RAZA)registro.DataBoundItem;

                string sql = "Delete from RAZA where ID_RAZA =" + raza.ID.ToString();

                if (acceso.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error. Verificar que la raza a borrar no esta siendo ocupada por un personaje.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Ok");
                }

            }

            EnlazarRaza();
            acceso = null;
            GC.Collect();
        }
    }
}
