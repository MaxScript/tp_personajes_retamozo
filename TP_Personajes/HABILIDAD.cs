﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TP_Personajes
{
    class HABILIDAD
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int mana;

        public int Mana
        {
            get { return mana; }
            set { mana = value; }
        }
        private int daño;

        public int Daño
        {
            get { return daño; }
            set { daño = value; }
        }
        public static List<HABILIDAD> ListarHabilidad()
        {
            ACCESO acceso = new ACCESO();
            List<HABILIDAD> habilidades = new List<HABILIDAD>();

            string sql = "SELECT * FROM HABILIDAD";

            acceso.Abrir();

            SqlDataReader lector = acceso.Leer(sql);

            while (lector.Read())
            {
                HABILIDAD habilidad = new HABILIDAD();

                habilidad.ID = lector.GetInt32(0);
                habilidad.Nombre = lector.GetString(1);
                habilidad.Mana = lector.GetInt32(2);
                habilidad.Daño = lector.GetInt32(3);
                
                habilidades.Add(habilidad);
            }

            lector.Close();

            acceso.Cerrar();

            return habilidades;
        }
        public static void ListarHabilidad(PERSONAJE personaje)
        {
            ACCESO acceso = new ACCESO();
            personaje.Habilidades.Clear();

            string sql = "Select * from HABILIDAD join PERSONAJE_HABILIDAD on HABILIDAD.ID_HABILIDAD = PERSONAJE_HABILIDAD.ID_HABILIDAD";
            sql += " where personaje_habilidad.id_personaje = "+ personaje.ID_Personaje.ToString();
            acceso.Abrir();

            SqlDataReader lector = acceso.Leer(sql);

            while (lector.Read())
            {
                HABILIDAD habilidad = new HABILIDAD();

                habilidad.ID = lector.GetInt32(0);
                habilidad.Nombre = lector.GetString(1);
                habilidad.Mana = lector.GetInt32(2);
                habilidad.Daño = lector.GetInt32(3);

                personaje.Habilidades.Add(habilidad);
            }

            lector.Close();

            acceso.Cerrar();

            
        }

        public void Insertar(PERSONAJE personaje)
        {
            ACCESO acceso = new ACCESO();

            //string sql = "SELECT ISNULL (MAX(ID_RAZA),0) + 1 FROM RAZA";

            //this.id = acceso.DevolverEscalar(sql);

            string sql = "INSERT INTO PERSONAJE_HABILIDAD (ID_PERSONAJE,ID_HABILIDAD) VALUES (" + personaje.ID_Personaje.ToString() + ", '" + this.id.ToString() + "')";

            acceso.Escribir(sql);

        }
        public void Insertar()
        {
            ACCESO acceso = new ACCESO();

            //string sql = "SELECT ISNULL (MAX(ID_RAZA),0) + 1 FROM RAZA";

            //this.id = acceso.DevolverEscalar(sql);

            string sql = "INSERT INTO HABILIDAD (NOMBRE, MANA, DAÑO) VALUES ('" + this.Nombre + "', " + this.mana.ToString() +", "+ this.Daño.ToString() +")";

            acceso.Escribir(sql);

        }
        public override string ToString()
        {
            return nombre;
        }
    }
}
