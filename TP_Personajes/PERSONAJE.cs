﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace TP_Personajes
{
    class PERSONAJE
    {
        private int id_personaje;

        public int ID_Personaje
        {
            get { return id_personaje; }
            set { id_personaje = value; }
        }
        private string nombre_p;

        public string Nombre_p
        {
            get { return nombre_p; }
            set { nombre_p = value; }
        }

        private int fuerza;

        public int Fuerza
        {
            get { return fuerza; }
            set { fuerza = value; }
        }

        private int nivel;

        public int Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private int experiencia;

        public int Experiencia
        {
            get { return experiencia; }
            set { experiencia = value; }
        }

        private RAZA raza;

        public RAZA Raza
        {
            get { return raza; }
            set { raza = value; }
        } 

        private List<HABILIDAD> habilidades = new List<HABILIDAD>();

        public List<HABILIDAD> Habilidades
        {
            get { return habilidades; }
            
        }

        public void ObtenerHabilidades()
        {
            HABILIDAD.ListarHabilidad(this);

        }

        public static List<PERSONAJE> ListarPersonajes()
        {
            ACCESO acceso = new ACCESO();
            List<PERSONAJE> personajes = new List<PERSONAJE>();

            List<RAZA> razas = RAZA.ListarRaza();
            

            string sql = "Select * from personaje";

            acceso.Abrir();

            SqlDataReader lector = acceso.Leer(sql);

            while (lector.Read())
            {
                PERSONAJE personaje = new PERSONAJE();
                personaje.ID_Personaje = int.Parse(lector["id_personaje"].ToString());
                personaje.Nombre_p = lector.GetString(1);
                personaje.Fuerza = int.Parse(lector["fuerza"].ToString());
                personaje.Nivel = int.Parse(lector["nivel"].ToString());
                personaje.Puntos = int.Parse(lector["puntos"].ToString());
                personaje.Experiencia = int.Parse(lector["experiencia"].ToString());

                int indice = 0;
                while (personaje.Raza == null)
                {

                    if (lector.GetInt32(6) == razas[indice].ID)
                    {
                        personaje.Raza = razas[indice];
                    }
                    else
                    {
                        indice++;
                    }
                }

                personajes.Add(personaje);
            }


            lector.Close();

            acceso.Cerrar();

            return personajes;
        }
        public void Insertar()
        {
            ACCESO acceso = new ACCESO();

            string sql = "SELECT ISNULL (MAX(ID_PERSONAJE),0) + 1 FROM PERSONAJE";

            this.id_personaje = acceso.DevolverEscalar(sql);

            sql = "INSERT INTO PERSONAJE (NOMBRE_P, Fuerza, Puntos, Nivel, Experiencia, ID_RAZA) VALUES ";

            sql += "('"+ this.Nombre_p.ToString() + "', "+this.Fuerza.ToString() + ", " + this.Puntos.ToString() + ", " + this.Nivel.ToString() + ", " + this.Experiencia.ToString() + ", " + this.Raza.ID.ToString() +")";

            acceso.Escribir(sql);

        }
    }
}
