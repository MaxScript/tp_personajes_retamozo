﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TP_Personajes
{
    class RAZA
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre_raza;

        public string Nombre_Raza
        {
            get { return nombre_raza; }
            set { nombre_raza = value; }
        }

        public override string ToString()
        {
            return nombre_raza;
        }

        public static List<RAZA> ListarRaza() 
        {
            ACCESO acceso = new ACCESO();
            List<RAZA> razas = new List<RAZA>();

            string sql = "SELECT * FROM RAZA";

            acceso.Abrir();

            SqlDataReader lector = acceso.Leer(sql);

            while (lector.Read())
            {
                RAZA raza = new RAZA();

                raza.ID = lector.GetInt32(0);
                raza.Nombre_Raza = lector.GetString(1);

                razas.Add(raza);
            }

            lector.Close();

            acceso.Cerrar();

            return razas;
        }
        public void Insertar()
        {
            ACCESO acceso = new ACCESO();

            string sql = "SELECT ISNULL (MAX(ID_RAZA),0) + 1 FROM RAZA";

            this.id = acceso.DevolverEscalar(sql);
            
            sql = "INSERT INTO RAZA (ID_RAZA,NOMBRE_RAZA) VALUES (" + this.id.ToString() + ", '" + this.nombre_raza + "')";

            acceso.Escribir(sql);

        }
    }
}
